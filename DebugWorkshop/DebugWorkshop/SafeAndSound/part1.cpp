#include "part1.h"
#include <iostream>

char* string_copy(char* dest, unsigned int destsize, char* src)
{
	char* ret = dest;

	while (destsize > 1 && (*dest++ = *src++))  //checking distance
	{
		destsize--;
	}

	*dest = '\0'; //adding string charecter

	return ret;
}

void part1()
{
	char password[] = "secret";
	char dest[13]; //adding one more space for string charecter
	char src[] = "hello world!";

	string_copy(dest, 13, src);

	std::cout << src << std::endl;
	std::cout << dest << std::endl;
}
