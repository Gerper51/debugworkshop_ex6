#include <iostream>

struct Password
{
	char value[16];
	bool incorrect;
	Password() : value(""), incorrect(true)
	{
	}
};

int main()
{
	std::cout << "Enter your password to continue:" << std::endl;
	Password pwd;
	std::cin >> pwd.value;

	if (!strcmp(pwd.value, "********"))
		pwd.incorrect = false;

	if(!pwd.incorrect)
		std::cout << "Congratulations\n";

	return 0;
}
  /*
  The problem is simple, we have a buffer that is allocated 16 places now if we fill them all with numbers
  we will suddenly get a acsses to a files, in out case to a word COngratulations.
  Example for input: 1234567891234567
  */